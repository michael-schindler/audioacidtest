function [DevIn, DevOut] = uigetaudioio(varargin)
%%
% UIGETAUDIOIO
% Opens a window, in which the audio in- and output devices may be choosen.
% Call example:
%
%   [DevIn, DevOut] = uigetaudioio()
%
%  See also UIGETFILE

    %define standard arguments
    figureWidthPx=500;%fixed figure width in px
    
    h_figure = figure('name', 'Audio IO Control', ...
        'position', [500 400 figureWidthPx 200], ...
        'resize','off', ...
        'windowstyle','modal', ...
        'menubar','none', ...
        'toolbar','none', ...
        'numbertitle','off');

    AudioDevInfo = audiodevinfo;%collect audio device info

    h_PanelOut = uipanel ('parent',h_figure,...
        'title', 'Audio Output Device', ...
        'units', 'pixels', ...
        'position', [10 130 figureWidthPx-20 60]);
    h_PopUpOut = uicontrol (h_PanelOut, 'style', 'popupmenu', ...
        'position',[10 10 figureWidthPx-40 25]);

    for i=1:size(AudioDevInfo.output,2)
        set(h_PopUpOut,'string',[get(h_PopUpOut,'String');{AudioDevInfo.output(i).Name}])
    end

    try
        set(h_PopUpOut,'value',1);
        DevOut = AudioDevInfo.output(1);
    catch ME
        switch ME.identifier
            case 'MATLAB:badsubscript'
                disp 'No output device found';
                DevIn = struct([]);
                close(h_figure);
                return;
            otherwise
                rethrow(ME);
        end
    end

    h_PanelIn = uipanel ('parent',h_figure, ...
        'title', 'Audio Input Device', ...
        'units', 'pixels', ...
        'position', [10 60 figureWidthPx-20 60]);
    h_PopUoIn = uicontrol (h_PanelIn, 'style', 'popupmenu', ...
        'position',[10 10 figureWidthPx-40 25]);

    for i=1:size(AudioDevInfo.input,2)
        set(h_PopUoIn,'string',[get(h_PopUoIn,'String');{AudioDevInfo.input(i).Name}])
    end
    
    try
        set(h_PopUoIn,'value',1);
        DevIn = AudioDevInfo.input(1);
    catch ME
        switch ME.identifier
            case 'MATLAB:badsubscript'
                disp 'No input device found';
                DevIn = struct([]);
                close(h_figure);
                return;
            otherwise
                rethrow(ME);
        end
    end

    h_PushButtonOK = uicontrol (h_figure, 'style', 'pushbutton', ...
        'string', 'OK', ...
        'position',[10 10 figureWidthPx-20 40]);
    set(h_PushButtonOK, 'callback', 'uiresume(gcbf)');

    uiwait(h_figure);
    
    DevIn = AudioDevInfo.input(get(h_PopUoIn,'value'));
    DevOut = AudioDevInfo.output(get(h_PopUpOut,'value'));
    
    close(h_figure);
end