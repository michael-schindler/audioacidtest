%%
% AUDIOACIDTEST
% This script runs the audio acid test. It will create some signals and put them
% through the DUT. Aftewards it will ask to save the file and evaluate/compute
% characteristic values of the tested system.
%
% Additional information, formula and explanation for chirp signal generator:
% <a href="https://www.gaussianwaves.com/2014/07/chirp-signal-frequency-sweeping-fft-and-power-spectral-density/">Link</a>

Fs=48e3;%sample frequency in Hz

[RecDev, SigDev] = uigetaudioio();

if isempty(RecDev) || isempty(SigDev)
    disp('Device not found');
    return;
end

%configuration info
SigDuration = 4;%signal duration in s
SigCut = [0 0];%time cut on beginning and end of record in s

SigT=[0:1/Fs:SigDuration];%signal time

sigform = 'sweep';

switch sigform
    case 'sine'
        %set default parameters
        f0 = 1e3;%sine frequency in Hz
        SigF = sin(2*pi*f0*SigT);%sine
        
        SigCut = [0.0 .2];%cut mute on begin and end of record
    case {'chirp', 'sweep'}
        %set default parameters
        T = SigDuration;%period of sweep in s
        f0 = 20;%start frequency in Hz
        f1 = 20e3;%stop frequency in Hz
        
        %SigF = sin(2*pi*((f1-f0).*mod(SigT,T)/T/2+f0).*SigT);
        SigF = chirp(SigT,f0,T,f1,'linear');
    case 'file'
        [ifile,ipath] = uigetfile('*.wav');
        if ifile == 0
            return
        end
        [SigF,SigFs] = audioread([ipath ifile]);
        SigDuration = size(SigF,1)/SigFs;
        SigT = (1:size(SigF,1))/SigFs;
    otherwise
        disp('signal form not found');
        SigF = zeros(size(SigT));%no signal
end

%signal generator
SigFs = Fs;%signal sample frequency in Hz
SigRes = 24;%signal resolution in bits
SigPlayer = audioplayer(SigF,SigFs,SigRes,SigDev.ID);%audioplayer object

%recorder generator
RecFs = Fs;%recording sample rate in Hz
RecRes = 24;%recording resolution in bits
RecRecord = audiorecorder(RecFs,RecRes,2,RecDev.ID);%stereo channel recording at default device

%signal measurement - TIME CRITICAL
play(SigPlayer);
while ~isplaying(SigPlayer)%wait until signal player is actually playing
end
recordblocking(RecRecord,SigDuration);
stop(SigPlayer);
%signal measurement - TIME CRITICAL

%get audio data
y = getaudiodata(RecRecord);

%cut audio signals
y = y(SigCut(1)*RecFs+1:size(y,1)-SigCut(2)*RecFs,:);

%save as audio-file
[file,path] = uiputfile('*.wav','Testdatei speichern','rec_meas');

if file == 0
    return
end

audiowrite([path file],y,RecFs);

filename = strsplit(file,'.');

switch sigform
    case 'sine'
        %analyze recorded data
        if exist('OCTAVE_VERSION', 'builtin') ~= 0
            result=struct(...
              'name',file,...
              'path',path,...
              'f0',SigFs,...
              'L','unknown',...
              'output',SigDev.Name,...
              'out_bitpsa',SigPlayer.BitsPerSample,...
              'out_fs',SigPlayer.SampleRate,...
              'out_numchan',SigPlayer.NumberOfChannels,...
              'input',RecDev.Name,...
              'in_bitpsa',RecRecord.BitsPerSample,...
              'in_fs',RecRecord.SampleRate,...
              'in_numchan',RecRecord.NumberOfChannels,...
              'CH1','channel1',...
              'CH1_MAX',max(y(:,1)),...
              'CH1_RMS',sqrt(mean(y(:,1).^2)),...
              'CH2','channel2',...
              'CH2_MAX',max(y(:,2)),...
              'CH2_RMS',sqrt(mean(y(:,2).^2))...
            );

            struct2cell(result);
        else
            result=struct(...
              'name',file,...
              'path',path,...
              'f0',SigFs,...
              'L','unknown',...
              'output',SigDev.Name,...
              'out_bitpsa',SigPlayer.BitsPerSample,...
              'out_fs',SigPlayer.SampleRate,...
              'out_numchan',SigPlayer.NumberOfChannels,...
              'input',RecDev.Name,...
              'in_bitpsa',RecRecord.BitsPerSample,...
              'in_fs',RecRecord.SampleRate,...
              'in_numchan',RecRecord.NumberOfChannels,...
              'CH1','channel1',...
              'CH1_THD',thd(y(:,1),Fs),...
              'CH1_SNR',snr(y(:,1),Fs),...
              'CH1_SINAD',sinad(y(:,1),Fs),...
              'CH1_MAX',max(y(:,1)),...
              'CH1_RMS',sqrt(mean(y(:,1).^2)),...
              'CH2','channel2',...
              'CH2_THD',thd(y(:,2),Fs),...
              'CH2_SNR',snr(y(:,2),Fs),...
              'CH2_SINAD',sinad(y(:,2),Fs),...
              'CH2_MAX',max(y(:,2)),...
              'CH2_RMS',sqrt(mean(y(:,2).^2))...
            );

            temp_table = struct2table(result');
            if verLessThan('matlab', '9.0')
                writetable(temp_table,char(strcat(path,filename(1),'.xlsx')));
            else
                writetable(temp_table,string(strcat(path,filename(1),'.xlsx')));
            end
        end
    case {'chirp', 'sweep'}
        %plot and save bode diagram
        X = fft(SigF);%fourier transformation of input signal
        Y = fft(y(:,1)');%fourier transformation of output signal (channel 1)
        
        X_SS = X(:,floor(1:size(X,2)/2));%single sided spectrum
        fx_SS= SigFs*(1:size(X_SS,2))/2/size(X_SS,2);%single sided frequency
        
        Y_SS = Y(:,floor(1:size(Y,2)/2));%single sided spectrum
        f_SS = RecFs*(1:size(Y_SS,2))/2/size(Y_SS,2);%single sided frequency
        
        bode = figure();
        
        plot(fx_SS,20*log10(abs(X_SS)),f_SS,20*log10(abs(Y_SS)));
        
        xlabel('f in Hz');
        ylabel('|A(f)| in dB_{fs}');
        legend('transmitted','recorded');
        
        if verLessThan('matlab','9.0')
            savefig(bode,char(strcat(path,filename(1),'.fig')));
        else
            savefig(bode,string(strcat(path,filename(1),'.fig')));
        end
    otherwise
end