# AudioAcidTest

As the name says, this project is meant to test audio devices on their values. This code is meant to run in Matlab as well as in GNU Octave.

Just run the script and look if it fits your requirements.

In the current version, you have to modify the script manually to perform your required measurement. Soon there will be a GUI guiding you.

A little extra thougth, I want to share (feel free to comment), is to re-write this code in Python, so the compatibility problems should never matter. Coding just this little hack has been cumbersome whilst complying the compatibility from MATLAB and Octave. (Beware of making this compatible to SciLab as well!) Just let me know, what you think.

## Documentation style

For proper documentation, there are several guidelines in the internet. I will use the [recommendation of the columbia university](https://www.ee.columbia.edu/~marios/matlab/MatlabStyle1p5.pdf), a half-assed hungarian notation for when I think it's adequate. Usually I'd prefer a documentation in doxygen-style, but I think it is not neccessary, rather it might confuse the user. As soon as I think it is reasonable, I will put up a more detailed in-line documentation. Until then, the MATLAB in-line documentation and help documentation have to be sufficient.

## Known issues / FAQ

As I try to satisfy all needs, still some issues might occur. This section is (as long as there is no FAQ just recently) meant as a quick help sheet.

### Audio driver corrupted after script failed

If you are using a debian derivate (e.g. ubuntu) and you are using PulseAudio, the audio driver might not be handled properly by GNU Octave.

```
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.rear
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.center_lfe
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.side
ALSA lib pcm_route.c:867:(find_matching_chmap) Found no matching channel map
ALSA lib pcm_route.c:867:(find_matching_chmap) Found no matching channel map
ALSA lib pcm_route.c:867:(find_matching_chmap) Found no matching channel map
ALSA lib pcm_route.c:867:(find_matching_chmap) Found no matching channel map
Cannot connect to server socket err = Datei oder Verzeichnis nicht gefunden
Cannot connect to server request channel
jack server is not running or cannot be started
JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock
JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock
```
Then you might want to install `multimedia-jack`, as explained [here](https://askubuntu.com/questions/608480/alsa-problems-with-python2-7-unable-to-open-slave). It fixed the problem for me.